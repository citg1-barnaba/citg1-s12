package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    // Create User
    public void createUser(User user){
        userRepository.save(user);
    }

    // Get All user
    public Iterable<User> getUser(){
        return userRepository.findAll();
    }

    // Update a User
    public ResponseEntity updateUser(Long id, User user){
        User userForUpdate = userRepository.findById(id).get();

        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());

        userRepository.save(userForUpdate);

        return new ResponseEntity<>("User updated successfully!", HttpStatus.OK);
    }

    // Delete a User
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully!", HttpStatus.OK);
    }
}
