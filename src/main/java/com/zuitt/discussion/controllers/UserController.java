package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    //Create User
    @RequestMapping(value="/users", method= RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User Created Successfully", HttpStatus.CREATED);
    }

    // Get All Users
    @RequestMapping(value="/users", method=RequestMethod.GET)
    public ResponseEntity<Object> getUser(){
        return new ResponseEntity<>(userService.getUser(), HttpStatus.OK);
    }

    // Update a User
    @RequestMapping(value="/users/{userid}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long userid, @RequestBody User user){
        return userService.updateUser(userid, user);
    }

    // Delete a User
    @RequestMapping(value="/users/{userid}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid){
        return userService.deleteUser(userid);
    }
}
