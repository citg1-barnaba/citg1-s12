package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    // Create User
    void createUser(User user);

    // Get all users
    Iterable<User> getUser();

    // Update a User
    ResponseEntity updateUser(Long id, User user);

    // Delete a User
    ResponseEntity deleteUser(Long id);
}
